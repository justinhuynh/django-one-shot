from django.shortcuts import redirect, render, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos
    }
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail", id=create.id)
    else:
        form = TodoListForm()

    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "todo_object": todo_list,
        "form": form
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    model_instance = todo_list_delete.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect(request, "todos/todos.html",)

    return render(request, "todo_list_delete/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "items/create.html", context)
